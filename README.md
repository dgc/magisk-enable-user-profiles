# Enable User Profiles

This is a trivial module to apply system properties to enable user
profiles. Some device vendors disable profiles in their `build.prop`
files; enabling this module re-activates them.


# Building the Magisk module zipfile

Just use `make`. You need `python3` available.

	git clone https://gitlab.com/dgc/magisk-embargo
	cd magisk-embargo
	make
	adb push embargo.zip /sdcard/Download


# History

## 0.1 - 20211204

